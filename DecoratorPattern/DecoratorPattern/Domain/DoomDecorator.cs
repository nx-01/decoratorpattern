﻿using System;
namespace DecoratorPattern.Domain
{
    public class DoomDecorator : Weapon
    {
        private Weapon _decoratedWeapon;

        public DoomDecorator(Weapon decoratedWeapon)
        {
            _decoratedWeapon = decoratedWeapon;
        }

        public override string DoDamage()
        {
            var decoratedWeaponDamage = _decoratedWeapon.DoDamage();
            var doomDamage = "Of doom. ";
            return decoratedWeaponDamage + doomDamage;
        }
    }
}
