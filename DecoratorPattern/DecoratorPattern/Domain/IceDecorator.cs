﻿using System;
namespace DecoratorPattern.Domain
{
    public class IceDecorator : Weapon
    {
        private Weapon _decoratedWeapon;
        
        public IceDecorator(Weapon decoratedWeapon)
        {
            _decoratedWeapon = decoratedWeapon;
        }

        public override string DoDamage()
        {
            var decoratedWeaponDamage = _decoratedWeapon.DoDamage();
            var icyWeaponDamage = "2 ice damage. ";
            return decoratedWeaponDamage + icyWeaponDamage;
        }
    }
}
