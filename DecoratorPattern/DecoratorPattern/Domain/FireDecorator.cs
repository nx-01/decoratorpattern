﻿using System;
namespace DecoratorPattern.Domain
{
    public class FireDecorator : Weapon
    {
        private Weapon _weapon;
        
        public FireDecorator(Weapon decoratedWeapon)
        {
            _weapon = decoratedWeapon;
        }

        public override string DoDamage()
        {
            var decoratedWeaponDamage = _weapon.DoDamage();
            var fieryWeaponDamage = "1 fire damage. ";
            return decoratedWeaponDamage + fieryWeaponDamage;
        }
    }
}
