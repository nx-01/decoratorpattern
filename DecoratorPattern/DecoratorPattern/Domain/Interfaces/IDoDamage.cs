﻿using System;
namespace DecoratorPattern.Domain.Interfaces
{
    public interface IDoDamage
    {
        string DoDamage();
    }
}
