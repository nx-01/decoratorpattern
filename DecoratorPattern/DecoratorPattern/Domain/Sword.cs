﻿using System;
namespace DecoratorPattern.Domain
{
    public class Sword : Weapon
    {
        public Sword()
        {
        }

        public override string DoDamage()
        {
            return "5 damage. ";
        }
    }
}
