﻿using System;
using DecoratorPattern.Domain.Interfaces;

namespace DecoratorPattern.Domain
{
    public abstract class Weapon : IDoDamage
    {
        public abstract string DoDamage();      
    }
}
