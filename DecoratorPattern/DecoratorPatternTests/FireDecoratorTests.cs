﻿using System;
using DecoratorPattern.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPatternTests
{
    [TestClass]
    public class FireDecoratorTests
    {
        [TestMethod]
        public void FireDecorator_Adds_OneFireDamage()
        {
            Sword sword = new Sword();
            FireDecorator fierySword = new FireDecorator(sword);

            var damage = fierySword.DoDamage();

            Assert.IsTrue(damage.EndsWith("1 fire damage. "));
        }

        [TestMethod]
        public void FierySword_Adds_OneFireDamage()
        {
            Sword sword = new Sword();
            FireDecorator fierySword = new FireDecorator(sword);

            var damage = fierySword.DoDamage();

            Assert.AreEqual("5 damage. 1 fire damage. ", damage);
        }
    }
}
