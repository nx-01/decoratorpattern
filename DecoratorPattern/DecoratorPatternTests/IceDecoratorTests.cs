﻿using System;
using DecoratorPattern.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPatternTests
{
    [TestClass]
    public class IceDecoratorTests
    {
        [TestMethod]
        public void IceDecorator_Adds_OneFireDamage()
        {
            Sword sword = new Sword();
            IceDecorator iceySword = new IceDecorator(sword);

            var damage = iceySword.DoDamage();

            Assert.IsTrue(damage.EndsWith("2 ice damage. "));
        }

        [TestMethod]
        public void IceySword_Adds_TwoIceDamage()
        {
            Sword sword = new Sword();
            IceDecorator iceySword = new IceDecorator(sword);

            var damage = iceySword.DoDamage();

            Assert.AreEqual("5 damage. 2 ice damage. ", damage);
        }

        [TestMethod]
        public void IceyFierySword_Adds_TwoIceDamageAnd1FireDamage()
        {
            Sword sword = new Sword();
            IceDecorator iceySword = new IceDecorator(sword);
            FireDecorator iceyFierySword = new FireDecorator(iceySword);

            var damage = iceyFierySword.DoDamage();

            Assert.AreEqual("5 damage. 2 ice damage. 1 fire damage. ", damage);
        }
       
    }
}
