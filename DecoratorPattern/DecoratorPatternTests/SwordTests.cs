﻿using System;
using DecoratorPattern.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPatternTests
{
    [TestClass]
    public class SwordTests
    {
        [TestMethod]
        public void Sword_Does_FiveDamage()
        {
            Sword sword = new Sword();
            var damage = sword.DoDamage();

            Assert.AreEqual("5 damage. ", damage);
        }
    }
}
