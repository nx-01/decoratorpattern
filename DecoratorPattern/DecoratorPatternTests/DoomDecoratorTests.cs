﻿using System;
using DecoratorPattern.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPatternTests
{
    [TestClass]
    public class DoomDecoratorTests
    {
        [TestMethod]
        public void DoomDecorator_Adds_OneFireDamage()
        {
            Sword sword = new Sword();
            DoomDecorator doomSword = new DoomDecorator(sword);

            var damage = doomSword.DoDamage();

            Assert.IsTrue(damage.EndsWith("Of doom. "));
        }

        [TestMethod]
        public void DoomSword_Adds_Doooooooooom()
        {
            Sword sword = new Sword();
            DoomDecorator doomSword = new DoomDecorator(sword);

            var damage = doomSword.DoDamage();

            Assert.AreEqual("5 damage. Of doom. ", damage);
        }

        [TestMethod]
        public void IceyFieryDoomSword_EndsAllThings()
        {
            Sword sword = new Sword();
            IceDecorator iceySword = new IceDecorator(sword);
            FireDecorator iceyFierySword = new FireDecorator(iceySword);
            DoomDecorator theHolyHandGrenade = new DoomDecorator(iceyFierySword);

            var damage = theHolyHandGrenade.DoDamage();

            Assert.AreEqual("5 damage. 2 ice damage. 1 fire damage. Of doom. ", damage);
        }
    }
}
